# Variables: samtools, blasr
#       genome, person

import os
import itertools

for key, value in config.items():
    globals()[key.upper()] = value

# Input
#     genome/{genome}.fa
#     data/{person}/{genome}.reads.fq
# Output
#     data/{person}/{genome}.blasr_before.bam
#     blasr_tmp/{person}/{genome}/*


THREADS = 8

include: "common.smk"

rule all:
    input:
        'data/{PERSON}/{GENOME}.blasr_before.bam.bai'\
            .format(PERSON=PERSON, GENOME=GENOME)


rule merge_results:
    input:
        expand('blasr_tmp/{{person}}/{{genome}}/{shift}.bam', shift=range(THREADS)),
    output:
        'data/{person,[^.]+}/{genome,[^.]+}.blasr_before.bam'
    shell:
        '{SAMTOOLS} merge {output} {input}'


rule sort_bam:
    input: '{path}.unsorted.bam',
    output: '{path}.bam',
    shell: '{SAMTOOLS} sort -o {output} {input}'


rule run_blasr:
    input:
        reads = 'blasr_tmp/{person}/{genome}/{shift}.fq',
        genome = 'genome/{genome}.fa',
    output:
        'blasr_tmp/{person,[^.]+}/{genome,[^.]+}/{shift,[^.]+}.unsorted.bam',
    shell:
        'time {BLASR} {input.reads} {input.genome} --hitPolicy allbest --out - --sam --nproc 1 | '
        '{SAMTOOLS} view -b > {output}'


rule fastq_subset:
    input:
        'data/{person}/{genome}.reads.fq'
    output:
        'blasr_tmp/{person,[^.]+}/{genome,[^.]+}/{shift,[^.]+}.fq',
    shell:
        "awk 'int((NR - 1) / 4) % {THREADS} == {wildcards.shift}' {input} > {output}"
