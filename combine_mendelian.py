#!/usr/bin/env python3

import re


def perc_str(part, total):
    return '{:.2f}'.format(100 * part / total if total != 0 else 0.0)


def analyze_report(inp, groups, outp):
    report_group = None
    line = next(inp)
    for group in groups:
        if re.search(group, line):
            assert report_group is None
            report_group = group
            break

    m = re.search(r'segdup_(\d+)-(\d+)\.', line)
    assert m is not None
    lower = int(m.group(1))
    upper = int(m.group(2))

    next(inp)
    concordance = next(inp)
    m = re.search(r'F\+M:(\d+)/(\d+) ', concordance)
    assert m is not None
    confirm = int(m.group(1))
    total = int(m.group(2))

    outp.write('%s\t%d\t%d\t%d\t%d\t%s\n' % (report_group, lower, upper, confirm, total,
        perc_str(confirm, total)))


def main():
    import argparse
    parser = argparse.ArgumentParser(
        description='Combine mendelian reports',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i FILE [FILE*] -o FILE')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='FILE', required=True, nargs='+',
        help='Multiple files with reports', type=argparse.FileType())
    io_args.add_argument('-o', '--output', metavar='FILE', required=True,
        help='Output csv file', type=argparse.FileType('w'))

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('-g', '--groups', metavar='STR', nargs='*', default=[],
        help='Groups (are matched against the first line in a report)')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    args.output.write('group\tlower_sim\tupper_sim\tconfirm\ttotal\tperc\n')
    for inp in args.input:
        analyze_report(inp, args.groups, args.output)


if __name__ == '__main__':
    main()
