# Variables:
#       genome, person, aligner

import os
import pysam

for key, value in config.items():
    globals()[key.upper()] = value

DIR = os.path.dirname(workflow.snakefile)

# Input
#     genome/{genome}.fa
#     data/{person}/{genome}.{aligner}_{flag}.bam
#     realignment/{person}/{genome}.{aligner}/psvs.vcf.gz
# Output
#     data/{person}/{genome}.{aligner}.(reads|psvs).csv
#     tmp/conflicts/{person}.{genome}.{aligner}/*


EXTENSION = ''

def extract_chromosomes():
    path = 'realignment/%s/%s.%s/psvs.vcf' % (PERSON, GENOME, ALIGNER)
    global EXTENSION
    if os.path.exists(path):
        vcf = pysam.VariantFile(path)
    else:
        EXTENSION = '.gz'
        vcf = pysam.VariantFile(path + '.gz')
    
    chroms = set()
    for record in vcf:
        chroms.add(record.chrom)

    return [chrom for chrom in vcf.header.contigs if chrom in chroms]

CHROMS = extract_chromosomes()


rule all:
    input:
        expand('data/{person}/{genome}.{aligner}.{type}.csv',
            person=PERSON, genome=GENOME, aligner=ALIGNER, type=['reads', 'psvs'])

rule combine:
    input:
        expand('tmp/conflicts/{{person}}.{{genome}}.{{aligner}}/{{type}}.{chrom}.csv',
            chrom=CHROMS),
    output:
        'data/{person,[^/]+}/{genome,[^.]+}.{aligner,[^.]+}.{type,[^.]+}.csv'
    shell:
        '''
        head -n 2 {input[0]} > {output}
        for f in {input}; do
            tail -n+3 $f >> {output}
        done
        '''

rule run_chrom:
    input:
        before = 'data/{person}/{genome}.{aligner}_before.bam',
        after = 'data/{person}/{genome}.{aligner}_after.bam',
        psvs = 'realignment/{person}/{genome}.{aligner}/merged_psvs.csv',
        genome = 'genome/{genome}.fa'
    output:
        reads = 'tmp/conflicts/{person}.{genome}.{aligner}/reads.{chrom}.csv',
        psvs = 'tmp/conflicts/{person}.{genome}.{aligner}/psvs.{chrom}.csv',
    shell:
        '"{DIR}"/count_conflicts.py -b before={input.before} after={input.after}'
            ' -p {input.psvs} -r {input.genome} -R {output.reads} -P {output.psvs}'
            ' -c {wildcards.chrom}'

rule merge_pvsv:
    input: 'realignment/{person}/{genome}.{aligner}/psvs.vcf%s' % EXTENSION,
    output:
        csv = 'realignment/{person}/{genome}.{aligner}/merged_psvs.csv',
        vcf = 'realignment/{person}/{genome}.{aligner}/merged_psvs.vcf',
    shell:
        '"{DIR}"/merge_psvs.py -i {input} -o {output.csv} -v {output.vcf}'

