#!/usr/bin/env python3

import sys
import re
import argparse
import sys
import gzip


def extract_feature(line, features, required=True):
    if isinstance(features, str):
        features = [features]
    for feature in features:
        m = re.search(feature + '=([^;]+)', line[8])
        if m is not None:
            return m.group(1)
    if required:
        sys.stderr.write('Cannot find features %s in line "%s"\n' % (features, line[8]))
        exit(1)


def to_entry(line, name):
    return (line[0], int(line[3]) - 1, int(line[4]), name, line[2])


def gene_match(line, genes, old_gene_entries):
    name = extract_feature(line, ['Name', 'gene_name'])
    if old_gene_entries is None:
        return name if name in genes else None

    descr = extract_feature(line, 'description', required=False)
    if not descr:
        return None
    gene_id = extract_feature(line, 'ID')
    if gene_id in old_gene_entries:
        return None

    for gene_name in genes:
        if gene_name in descr or name in gene_name:
            return gene_name
    return None


def process_annotation(annotation, genes, out_entries, old_gene_entries=None):
    gene_entries = {}
    mrna_entries = {}
    genes_not_found = set(genes)

    for line in annotation:
        if line.startswith('#'):
            continue
        line = line.strip().split('\t')

        if 'gene' in line[2]:
            gene_name = gene_match(line, genes, old_gene_entries)
            if gene_name is None:
                continue
            try:
                genes_not_found.remove(gene_name)
            except KeyError:
                pass
            gene_id = extract_feature(line, 'ID')
            assert gene_id not in gene_entries

            name = extract_feature(line, ['Name', 'gene_name'])
            gene_entries[gene_id] = name
            out_entries.append(to_entry(line, name))

        elif line[2] == 'mRNA' or line[2] == 'transcript':
            parent = extract_feature(line, 'Parent')
            if parent in gene_entries:
                name = gene_entries[parent]
                out_entries.append(to_entry(line, name))
                transcript_id = extract_feature(line, 'ID')
                assert transcript_id not in mrna_entries
                mrna_entries[transcript_id] = name

        elif line[2] == 'exon':
            parent = extract_feature(line, 'Parent')
            if parent in mrna_entries:
                name = mrna_entries[parent]
                out_entries.append(to_entry(line, name))
    return genes_not_found, gene_entries


def write_output(out_entries, fai_file, outp):
    chrom_id = {}
    for i, line in enumerate(fai_file):
        chrom_id[line.strip().split()[0]] = i
    out_entries.sort(key=lambda entry: (chrom_id.get(entry[0], sys.maxsize), entry[1], entry[2]))
    prev = None
    for entry in out_entries:
        if entry != prev:
            outp.write('%s\n' % '\t'.join(map(str, entry)))
        prev = entry


def open_annotation(filename):
    if filename == '-':
        return sys.stdin
    elif filename.endswith('.gz'):
        return gzip.open(filename, 'rt')
    else:
        return open(filename)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-g', '--genes', type=argparse.FileType(), metavar='FILE', required=True,
        help='Input gene names')
    parser.add_argument('-a', '--annotation', metavar='FILE', required=True,
        help='Input genome annotation')
    parser.add_argument('-f', '--fai', type=argparse.FileType(), metavar='FILE', required=True,
        help='Input genome fai index')
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), metavar='FILE', required=True,
        help='Output bed file with genes and exons')
    parser.add_argument('-n', '--not-found', type=argparse.FileType('w'), metavar='FILE',
        default=sys.stderr, help='Output file with names of not found genes [stderr]')
    parser.add_argument('-s', '--second-try', action='store_true',
        help='Try to find not previously found genes with less strict rules')
    args = parser.parse_args()

    genes = set(map(str.strip, args.genes))
    annotation = list(open_annotation(args.annotation))

    args.output.write('# %s\n' % ' '.join(sys.argv))

    out_entries = []
    genes_not_found, gene_entries = process_annotation(annotation, genes, out_entries, None)
    # if args.second_try:
    #     genes_not_found, _ = process_annotation(annotation, genes_not_found, out_entries,
    #         gene_entries)

    write_output(out_entries, args.fai, args.output)
    for gene in genes_not_found:
        args.not_found.write('%s\n' % gene)


if __name__ == '__main__':
    main()

