#!/usr/bin/env python3

import sys
import pysam
import argparse
import numpy as np
import csv


def compare_vcfs(psvs_vcf, variants_vcf, ref_fasta, outp):
    sample = variants_vcf.header.samples[0]

    psvs = [next(psvs_vcf)]
    for variant in variants_vcf:
        chrom = variant.chrom
        start = variant.start
        end = variant.stop
        gt = variant.samples[sample]['GT']

        intersecting = list(psvs_vcf.fetch(chrom, start, end))
        if not intersecting:
            outp.write(str(variant))
            continue

        row = dict(chrom=chrom, pos=start + 1, ref=variant.ref, alt=','.join(variant.alts),
            gt='/'.join(map(str, gt)))
        window_start = min(intersecting[0].start, start)
        window_end = max(max(psv.stop for psv in intersecting), end)
        ref = ref_fasta.fetch(chrom, window_start, window_end)
        variant_alts = [ref[:start - window_start] + alt + ref[end - window_start:]
            for alt in variant.alts]
        variant_alleles = [None if x == 0 else variant_alts[x - 1] for x in gt]
        was_alt = [False] * 2

        psvs_ids = set()
        for psv in intersecting:
            assert len(psv.alts) == 1
            alt = ref[:psv.start - window_start] + psv.alts[0] + ref[psv.stop - window_start:]
            for i in range(2):
                if alt == variant_alleles[i]:
                    was_alt[i] = True
            psvs_ids.add(psv.info['psv'])

        variant = str(variant).split('\t')
        variant[7] += ';intersects_psv=%s' % ','.join(sorted(psvs_ids))
        variant[7] += ';matches_psv=%s/%s' % (str(was_alt[0])[0], str(was_alt[1])[0])
        outp.write('\t'.join(variant))


def write_header(outp, header):
    was_info = False
    already_wrote = False
    for record in header.records:
        if record.key == 'INFO':
            was_info = True
        elif was_info and not already_wrote:
            outp.write('##INFO=<ID=intersects_psv,Number=.,Type=String,'
                'Description="Intersects PSVs">\n')
            outp.write('##INFO=<ID=matches_psv,Number=1,Type=String,Description="T or F depending '
                'if the variant matches one of the PSVs (once for each allele).">\n')
            already_wrote = True
        outp.write(str(record))
    outp.write('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT')
    for sample in header.samples:
        outp.write('\t%s' % sample)
    outp.write('\n')


def main():
    parser = argparse.ArgumentParser(description='Intersect VCF variants with PSVs')
    parser.add_argument('-p', '--psvs', metavar='FILE', required=True,
        help='Input sorted vcf file with PSVs')
    parser.add_argument('-v', '--variants', metavar='FILE', required=True,
        help='Input sorted vcf file with variants')
    parser.add_argument('-r', '--reference', metavar='FILE', required=True,
        help='Input reference in the fasta format')
    parser.add_argument('-o', '--output', metavar='FILE', required=True,
        help='Output vcf file', type=argparse.FileType('w'))
    args = parser.parse_args()

    psvs = pysam.VariantFile(args.psvs)
    variants = pysam.VariantFile(args.variants)

    ref_fasta = pysam.FastaFile(args.reference)
    write_header(args.output, variants.header)
    compare_vcfs(psvs, variants, ref_fasta, args.output)


if __name__ == '__main__':
    main()

