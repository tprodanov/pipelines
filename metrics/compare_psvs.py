#!/usr/bin/env python3

import argparse
import sys
import pysam
import csv


def load_vcf(filename):
    res = {}
    for record in pysam.VariantFile(filename):
        res[(record.chrom, record.start, record.info['psv'])] = record
    return res


def perc(num, den):
    if den == 0:
        return 'NA'
    else:
        return '%.1f' % (100 * num / den)


def compare_records(rec1, rec2, writer):
    s1 = rec1.samples['SAMPLE']
    s2 = rec2.samples['SAMPLE']
    row = dict(chrom=rec1.chrom, pos=rec1.pos, db=rec1.info['db'], psv=rec1.info['psv'],
        dp1=s1['DP'], dp2=s2['DP'], qual1='%.1f' % rec1.qual, qual2='%.1f' % rec2.qual,
        filt1=';'.join(rec1.filter), filt2=';'.join(rec2.filter))

    if None in s1['GT'] or None in s2['GT']:
        gt1 = str(rec1).split('\t')[-1].split(':')[0]
        gt2 = str(rec2).split('\t')[-1].split(':')[0]
    else:
        gt1 = '/'.join(map(str, s1['GT']))
        gt2 = '/'.join(map(str, s2['GT']))

    support1 = s1['sp']
    support2 = s2['sp']
    row.update(dict(gt1=gt1, gt2=gt2, match=gt1 == gt2,
        support1=','.join(map(str, support1)), support2=','.join(map(str, support2)),
        undecided1=perc(support1[2], sum(support1)),
        undecided2=perc(support2[2], sum(support2))),
        rel1='%.1f' % (100 * s1['rel']), rel2='%.1f' % (100 * s2['rel']))
    writer.writerow(row)


def compare(filename1, records2, writer):
    for rec1 in pysam.VariantFile(filename1):
        rec2 = records2.get((rec1.chrom, rec1.start, rec1.info['psv']))
        if rec2 is None:
            continue
        compare_records(rec1, rec2, writer)


def main():
    parser = argparse.ArgumentParser(description='Compare two PSVs VCF files')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-a', '--input-a', metavar='FILE', required=True,
        help='First input VCF file with PSVs')
    io_args.add_argument('-b', '--input-b', metavar='FILE', required=True,
        help='Second input VCF file with PSVs')
    io_args.add_argument('-o', '--output', metavar='FILE', required=True,
        help='Output CSV file',  type=argparse.FileType('w'))

    # opt_args = parser.add_argument_group('Optional arguments')
    # opt_args.add_argument('--a-dp', metavar='INT', type=int, default=10,
    #     help='Minimap read depth in the first VCF file [%(default)s]')
    # opt_args.add_argument('--b-dp', metavar='INT', type=int, default=10,
    #     help='Minimap read depth in the second VCF file [%(default)s]')
    # opt_args.add_argument('--a-qual', metavar='INT', type=int, default=30,
    #     help='Minimal quality in the first VCF file [%(default)s]')
    # opt_args.add_argument('--b-qual', metavar='INT', type=int, default=30,
    #     help='Minimal quality in the second VCF file [%(default)s]')
    args = parser.parse_args()

    args.output.write('# %s\n' % ' '.join(sys.argv))
    args.output.write('# support: reads that support allele: 0, 1, undecided\n')
    args.output.write('# undecided: 100 * support[2] / max(support[0], support[1])\n')
    writer = csv.DictWriter(args.output,
        fieldnames=('chrom pos db psv gt1 gt2 match support1 support2 undecided1 undecided2 ' +
            'dp1 dp2 qual1 qual2 filt1 filt2 rel1 rel2').split(),
        delimiter='\t', lineterminator='\n', quoting=csv.QUOTE_NONE)
    writer.writeheader()

    records2 = load_vcf(args.input_b)
    compare(args.input_a, records2, writer)


if __name__ == '__main__':
    main()
