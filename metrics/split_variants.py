#!/usr/bin/env python3

import argparse
import pysam
import numpy as np


def reconstruct(seq1, seq2, a):
    res1 = ''
    res2 = ''
    i = len(seq1)
    j = len(seq2)
    while i > 0 or j > 0:
        if i > 0 and j > 0 \
                and a[i, j] == a[i - 1, j - 1] + (1 if seq1[i - 1] != seq2[j - 1] else -1):
            res1 += seq1[i - 1]
            res2 += seq2[j - 1]
            i -= 1
            j -= 1
        elif i > 0 and a[i, j] == a[i - 1, j] + 1:
            res1 += seq1[i - 1]
            res2 += '-'
            i -= 1
        else:
            assert j > 0 and a[i, j] == a[i, j - 1] + 1
            res1 += '-'
            res2 += seq2[j - 1]
            j -= 1
    return res1[::-1], res2[::-1]


def align(seq1, seq2):
    m = len(seq1) + 1
    n = len(seq2) + 1
    a = np.zeros((m, n), dtype=int)
    a[:, 0] = range(m)
    a[0, :] = range(n)

    for i in range(1, m):
        for j in range(1, n):
            a[i, j] = min(a[i, j - 1] + 1, a[i - 1, j] + 1,
                a[i - 1, j - 1] + (1 if seq1[i - 1] != seq2[j - 1] else -1))
    return reconstruct(seq1, seq2, a)


def split_record(rec, output_records, split_subs):
    start = rec.start
    ref = rec.ref
    assert len(rec.alts) == 1
    alt = rec.alts[0]
    ref_aligned, alt_aligned = align(ref, alt)

    curr_start = start
    curr_ref = ''
    curr_alt = ''
    new_records = []
    for nt1, nt2 in zip(ref_aligned, alt_aligned):
        if nt1 == nt2 and curr_ref and curr_alt and curr_ref != curr_alt:
            new_rec = rec.copy()
            for i, (x, y) in enumerate(zip(curr_ref, curr_alt)):
                if x != y:
                    break
            else:
                i = min(len(curr_ref), len(curr_alt)) - 1
            new_rec.start = curr_start + i
            new_rec.ref = curr_ref[i:]
            new_rec.alts = [curr_alt[i:]]
            new_records.append(new_rec)

            curr_start += len(curr_ref)
            curr_ref = nt1
            curr_alt = nt2
            continue

        if nt1 != '-':
            curr_ref += nt1
        if nt2 != '-':
            curr_alt += nt2

    if curr_ref and curr_alt and curr_ref != curr_alt:
        new_rec = rec.copy()
        for i, (x, y) in enumerate(zip(curr_ref, curr_alt)):
            if x != y:
                break
        else:
            i = min(len(curr_ref), len(curr_alt)) - 1
        new_rec.start = curr_start + i
        new_rec.ref = curr_ref[i:]
        new_rec.alts = [curr_alt[i:]]
        new_records.append(new_rec)

    elif bool(curr_ref) != bool(curr_alt):
        new_records[-1].ref += curr_ref
        new_records[-1].alts = [new_records[-1].alts[0] + curr_alt]

    for record in new_records:
        if split_subs and len(record.ref) == len(record.alts[0]):
            for i, (nt1, nt2) in enumerate(zip(record.ref, record.alts[0])):
                if nt1 == nt2:
                    print(rec)
                    print(record)
                    assert False
                new_rec = record.copy()
                new_rec.start += i
                new_rec.ref = nt1
                new_rec.alts = [nt2]
                output_records.append(new_rec)
        else:
            output_records.append(record)


def process(in_filename, out_filename, split_subs):
    in_vcf = pysam.VariantFile(in_filename)
    out_vcf = pysam.VariantFile(out_filename, 'w', header=in_vcf.header)

    output_records = []
    for record in in_vcf:
        split_record(record, output_records, split_subs)
    output_records.sort(key=lambda record: (record.rid, record.start))
    for rec in output_records:
        out_vcf.write(rec)


def main():
    parser = argparse.ArgumentParser(description='Split variants')
    parser.add_argument('-i', '--input', metavar='FILE', required=True,
        help='Input vcf file')
    parser.add_argument('-o', '--output', metavar='FILE', required=True,
        help='Output vcf file')
    parser.add_argument('--split-subs', action='store_true',
        help='Split substitutions of the same length')
    args = parser.parse_args()
    process(args.input, args.output, args.split_subs)


if __name__ == '__main__':
    main()
