#!/usr/bin/env python3

import pysam
import pybedtools as bed
import argparse
import sys


def process_region(line, covered_regions, variants, outp):
    split_line = line.strip().split('\t')
    chrom = split_line[0]
    start = int(split_line[1])
    end = int(split_line[2])
    name = ','.join(split_line[3:]) if len(split_line) > 3 else '*'

    curr = bed.BedTool(line, from_string=True)
    covered_size = 0
    for record in curr.intersect(covered_regions, stream=True):
        covered_size += max(0, min(record.end, end) - max(record.start, start))
    outp.write('%s\t%d\t%d\t%s\t%d\t' % (chrom, start, end, name, covered_size))

    sample = variants.header.samples[0]
    n_variants = 0
    het_variants = 0
    inters_psv = 0
    matches_psv = 0
    het_psv = 0
    try:
        for variant in variants.fetch(chrom, start, end):
            gt = variant.samples[sample]['GT']
            if gt is None or gt.count(0) == 2:
                continue

            n_variants += 1
            if gt.count(0) == 1:
                het_variants += 1
            if 'intersects_psv' in variant.info:
                inters_psv += 1
                m = str(variant.info['matches_psv'])
                if 'T' in m:
                    matches_psv += 1
                if m.count('T') == 1:
                    het_psv += 1
        outp.write('%d\t%d\t%d\t%d\t%d\n' % (n_variants, het_variants,
            inters_psv, matches_psv, het_psv))
        outp.flush()
    except ValueError:
        outp.write('0\t0\t0\t0\t0\n')
        outp.flush()


def main():
    parser = argparse.ArgumentParser(description='Count variants intersecting the specified '
        'regions')
    parser.add_argument('-v', '--variants', metavar='FILE', required=True,
        help='Input vcf file with variants')
    parser.add_argument('-r', '--regions', metavar='FILE', required=True, type=argparse.FileType(),
        help='Input bed file with regions')
    parser.add_argument('-c', '--covered', metavar='FILE', required=True,
        help='Input bed file with singificantly covered regions')
    parser.add_argument('-o', '--output', metavar='FILE', required=True, type=argparse.FileType('w'),
        help='Output csv file')
    args = parser.parse_args()

    variants = pysam.VariantFile(args.variants)
    covered = bed.BedTool(args.covered)
    
    outp = args.output
    outp.write('# %s\n' % ' '.join(sys.argv))
    outp.write('#chrom\tstart\tend\tname\tcovered_size\tvariants\thet_variants\tinters_psv\t'
        'matches_psv\thet_psv\n')
    outp.flush()
    for line in args.regions:
        if line.startswith('#'):
            continue
        process_region(line, covered, variants, outp)


if __name__ == '__main__':
    main()
