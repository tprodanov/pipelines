# Variables: simlord, bcftools, samtools, minimap2
#       [length], coverage

import os

for key, value in config.items():
    globals()[key.upper()] = value

# Input
#     genome/{genome}.fa
# Output
#     simlord_tmp/{genome}_sim[_len{length}]_cov{coverage}/ref/*
#     simlord_tmp/{genome}_sim[_len{length}]_cov{coverage}/siml/*
#     data/{genome}_sim[_len{length}]_cov{coverage}/{genome}.truth.vcf.gz
#     data/{genome}_sim[_len{length}]_cov{coverage}/reads.fq
#     data/{genome}_sim[_len{length}]_cov{coverage}/{genome}.minimap_before.bam

include: "common.smk"
CHROM_PREFIX = get_chrom_prefix(GENOME)

DIR = os.path.dirname(workflow.snakefile)

# Selecting parameters of lognorm distribution
def select_params(length, svar=None):
    if svar is None:
        svar = length / 2.22
    s = 0.2
    scale = svar / 0.2061
    loc = length - 1.0202 * scale
    print('Using lognorm parameters: s={:.2}, loc={:.2}, scale={:.2}', s, loc, scale)
    return "-ln {s} {loc} {scale}".format(s=s, loc=loc, scale=scale)

if 'length' in config:
    LOGNORM = select_params(int(config['length']))
    LENGTH = "_len%dkb" % (LENGTH // 1000)
else:
    LOGNORM = ''
    LENGTH = ''

assert COVERAGE % 2 == 0
HALF_COVERAGE = COVERAGE // 2
assert HALF_COVERAGE > 0

chroms = [int(n + 1) for n in range(22)] + ['X', 'Y']

rule all:
    input:
        'data/{GENOME}_sim{LENGTH}_cov{COVERAGE}/{GENOME}.minimap_before.bam.bai'\
            .format(GENOME=GENOME, LENGTH=LENGTH, COVERAGE=COVERAGE)

rule sort_bam:
    input: '{path}.unsorted.bam',
    output: '{path}.bam',
    threads: 8,
    shell: '{SAMTOOLS} sort -@ {threads} -o {output} {input}'

rule align_minimap:
    input:
        genome = 'genome/{genome}.fa',
        fastq = 'data/{genome}_sim{length}_cov{coverage}/reads.fq'
    output:
        temp('data/{genome,[^.]+}_sim{length,(|_len\d+)}_cov{coverage,\d+}/{genome}.'\
            'minimap_before.unsorted.bam')
    threads: 8
    shell:
        'time {MINIMAP2} -t {threads} -ax map-pb --secondary=no {input.genome} {input.fastq} | '
            '{SAMTOOLS} view -b > {output}'

rule rename_reads:
    input:
        expand('simlord_tmp/{{genome}}_sim{{length}}_cov{{coverage}}/'\
            'siml/{chrom}.hapl{hapl}.sam', chrom=chroms, hapl=['1', '2']),
    output:
        'data/{genome}_sim{length,(|_len\d+)}_cov{coverage,\d+}/reads.fq'
    shell:
        '"{DIR}"/rename_reads.py -i {input} -o {output} 2> /dev/null'

rule generate_reads:
    input:
        'simlord_tmp/{genome}_sim{length}_cov{coverage}/ref/{chrom}.hapl{hapl}.fa',
    output:
        sam = 'simlord_tmp/{genome}_sim{length,(|_len\d+)}_cov{coverage}/siml/{chrom}.hapl{hapl}.sam',
    params:
        out_prefix = lambda wildcards, output: output[0][:-4]
    shell:
        '''
        {SIMLORD} -rr {input} {LOGNORM} -c {HALF_COVERAGE} -mp 1 {params.out_prefix}
        rm {params.out_prefix}.fastq
        if [ -f {params.out_prefix}.fastq.sam ]; then
            mv {params.out_prefix}.fastq.sam {params.out_prefix}.sam
        fi
        '''

rule generate_consensus:
    input:
        genome = 'genome/{genome}.fa',
        fa_index = 'genome/{genome}.fa.fai',
        vcf = 'data/{genome}_sim_{flag}/{genome}.truth.vcf.gz',
        tbi = 'data/{genome}_sim_{flag}/{genome}.truth.vcf.gz.tbi',
    output:
        'simlord_tmp/{genome}_sim_{flag}/ref/{chrom}.hapl{hapl}.fa',
    shell:
        '{SAMTOOLS} faidx {input.genome} {CHROM_PREFIX}{wildcards.chrom} | '
        '{BCFTOOLS} consensus -H {wildcards.hapl} {input.vcf} -o {output}'

rule generate_snvs:
    input:
        genome = 'genome/{genome}.fa',
        fa_index = 'genome/{genome}.fa.fai',
    output:
        'data/{genome}_sim_{flag}/{genome}.truth.vcf',
    shell:
        '"{DIR}"/simulate_snvs.py -i {input.genome} -o {output}'

