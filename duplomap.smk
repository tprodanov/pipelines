# Variables: duplomap, samtools
#       genome, person, aligner, generated [default: false], move

import os
import shutil
import itertools

for key, value in config.items():
    globals()[key.upper()] = value

generated = config.get('generated', 'false').lower()
if generated == 'true':
    GENERATED_FLAG = '--generated'
elif generated == 'false':
    GENERATED_FLAG = ''
else:
    print('Unexpected generated value', config['generated'])
    exit(1)

# Input
#     genome/{genome}.fa
#     db/{genome} (directory)
#     data/{person}/{genome}.{aligner}_before.bam
# Output
#     data/{person}/{genome}.{aligner}_after.bam
#     realignment/{person}/{genome}.{aligner}/*


def move_old():
    out_path = 'realignment/{person}/{genome}.{aligner}'\
        .format(person=PERSON, genome=GENOME, aligner=ALIGNER)
    realigned_bam = 'data/{person}/{genome}.{aligner}_after.bam'\
        .format(person=PERSON, genome=GENOME, aligner=ALIGNER)

    if not os.path.exists(out_path):
        return
    for i in itertools.count():
        new_path = '%s.old%d' % (out_path, i)
        if not os.path.exists(new_path):
            shutil.move(out_path, new_path)

            final_reads_path = os.path.join(new_path, 'final.sort.bam')
            if os.path.islink(final_reads_path):
                os.remove(final_reads_path)
                try:
                    shutil.move(realigned_bam, final_reads_path)
                except FileNotFoundError:
                    print('Failed to move old BAM file')
            if os.path.isfile(realigned_bam + '.bai') and \
                    (not os.path.exists(final_reads_path + '.bai') or 
                    os.path.islink(final_reads_path + '.bai')):
                shutil.move(realigned_bam + '.bai', final_reads_path + '.bai')
            break

if config.get('move') == 'true':
    move_old()

rule all:
    input:
        expand('data/{PERSON}/{GENOME}.{ALIGNER}_after.{extension}', \
            PERSON=PERSON, GENOME=GENOME, ALIGNER=ALIGNER, extension=['bam', 'bam.bai'])

rule move_output:
    input:
        'realignment/{person}/{genome}.{aligner}/final.sort{suffix}',
    output:
        'data/{person}/{genome}.{aligner}_after{suffix,.bam|.bam.bai}',
    shell:
        '''
        mv {input} {output}
        if [ "$(command -v realpath)" ]; then
            ln -s $(realpath {output}) {input}
        elif [ "$(command -v readlink)" ]; then
            ln -s $(readlink -f {output}) {input}
        else
            ln -s {output} {input}
        fi
        '''

rule run_realignment:
    input:
        bam = 'data/{person}/{genome}.{aligner}_before.bam',
        bai = 'data/{person}/{genome}.{aligner}_before.bam.bai',
        db = 'db/{genome}',
        genome = 'genome/{genome}.fa',
        fai = 'genome/{genome}.fa.fai',
    output:
        bam = 'realignment/{person}/{genome}.{aligner}/final.sort.bam',
        fai = 'realignment/{person}/{genome}.{aligner}/final.sort.bam.bai',
    params:
        out_dir = lambda wildcards, output: os.path.dirname(output[0])
    threads:
        8
    shell:
        'time {DUPLOMAP} -i {input.bam} -d {input.db} -r {input.genome} -o {params.out_dir} '
            '--samtools {SAMTOOLS} -@ {threads} {GENERATED_FLAG} --output-debug'
