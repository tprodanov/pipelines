# Variables: bedtools, tabix, rtgtools, samtools, [rtg_mem],
#       genome, bam_flag, vcf_flag
for key, value in config.items():
    globals()[key.upper()] = value

include: "common.smk"

RTG_MEM = config.get('rtg_mem', '12g')
CHROM_PREFIX = get_chrom_prefix(GENOME)

CHILD = 'HG002'
FATHER = 'HG003'
MOTHER = 'HG004'
TRIO = [CHILD, FATHER, MOTHER]
TRIO_NAME = 'aj_trio'

MIN_COV = 20
chroms = [int(n + 1) for n in range(22)] + ['X', 'Y']

SEGDUP_LEN = '10kb'
THRESHOLDS = [0] + list(range(900, 1000, 10)) + [995, 999, 1000]
BINS = ['%d-%d' % (THRESHOLDS[i], THRESHOLDS[i + 1]) for i in range(len(THRESHOLDS) - 1)]

# Input
#     data/{trio_name}.ped
#     data/{person}/{genome}.{bam_flag}.bam
#     data/{person}/{genome}.{bam_flag}.{vcf_flag}.vcf.gz
#     data/{person}/{genome}.confident_regions.bed
#     genome/{genome}.fa
#     genome/regions/{genome}.centromeres.bed
#     genome/segm_dup/{genome}.{length}.{bin}.bed'
# Output
#     genome/regions/{genome}.whole_genome.bed
#     trio/{genome}/*
#     trio/{genome}/_{person}|{trio_name}/*
#     trio/{genome}/_{person}|{trio_name}/{bam_flag}/*
#     trio/{genome}/_{person}|{trio_name}/{bam_flag}/{vcf_flag}/*
#     trio/reports/{trio_name}.{genome}.{bam_flag}.{vcf_flag}/*.txt'

rule all:
    input:
        expand('trio/{GENOME}/_{person}/{BAM_FLAG}/cov_{MIN_COV}.bed',\
            GENOME=GENOME, BAM_FLAG=BAM_FLAG, MIN_COV=MIN_COV, person=TRIO),
        expand('trio/reports/{TRIO_NAME}.{GENOME}.{BAM_FLAG}.{VCF_FLAG}/cov_{MIN_COV}.{region}.txt',\
            TRIO_NAME=TRIO_NAME, GENOME=GENOME, BAM_FLAG=BAM_FLAG,\
            VCF_FLAG=VCF_FLAG, MIN_COV=MIN_COV,\
            region=['whole_genome', 'confident', 'nonconfident'] \
                + ['segdup_%s' % bin for bin in BINS])

rule trio_mendelian:
    message: 'Run mendelian analysis for the {wildcards.region}'
    input:
        vcf = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.variants_{region}.vcf.gz',
        tbi = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.variants_{region}.vcf.gz.tbi',
        ped = 'data/{trio_name}.ped',
        sdf = 'genome/{genome}.sdf'
    output:
        vcf = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.mendelian_{region}.vcf.gz',
        report = 'trio/reports/{trio_name}.{genome}.{bam_flag}.{vcf_flag}/cov_{min_cov}.{region}.txt',
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} mendelian --lenient --pedigree={input.ped} -t {input.sdf} '
            '-i {input.vcf} --output={output.vcf} > {output.report}'

def select_bed(wildcards):
    if wildcards.region == 'whole_genome':
        return 'genome/regions/{genome}.whole_genome.bed'.format(genome=wildcards.genome)
    elif wildcards.region == 'confident' or wildcards.region == 'nonconfident':
        return 'trio/{genome}/{trio_name}/{region}.intersect.bed'\
            .format(genome=wildcards.genome, trio_name=wildcards.trio_name, region=wildcards.region)
    elif wildcards.region.startswith('segdup'):
        bin = wildcards.region.split('_')[1]
        return 'genome/segm_dup/{genome}.{length}.{bin}.bed'\
            .format(genome=wildcards.genome, length=SEGDUP_LEN, bin=bin)

rule select_by_regions:
    message: 'Select SNVs from the {wildcards.region}'
    input:
        vcf = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.wo_centromeres.vcf.gz',
        tbi = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.wo_centromeres.vcf.gz.tbi',
        regions = select_bed,
    output:
        'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.variants_{region}.vcf.gz'
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} vcffilter --include-bed={input.regions} -i {input.vcf} -o {output}'

rule discard_centromeres:
    message: 'Discard centromere and telomere regions'
    input:
        vcf = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.wo_failed.vcf.gz',
        tbi = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.wo_failed.vcf.gz.tbi',
        centromeres = 'genome/regions/{genome}.centromeres.bed',
    output:
        'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.wo_centromeres.vcf.gz',
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} vcffilter --exclude-bed={input.centromeres} '
            '-i {input.vcf} -o {output}'

rule remove_failed_from_filtered:
    message: 'Remove failed SNVs from filtered SNVs'
    input:
        filtered = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.filtered.vcf.gz',
        tbi = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.filtered.vcf.gz.tbi',
        failed = 'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.failed.vcf.gz',
    output:
        'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}.wo_failed.vcf.gz'
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} vcffilter --exclude-vcf={input.failed} -i {input.filtered} '
            '-o {output}'

rule merge_trio_snvs:
    message: 'Merge {wildcards.status} SNVs'
    input:
        vcfs = expand('trio/{{genome}}/_{person}/{{bam_flag}}/{{vcf_flag}}/cov_{{min_cov}}'
            '.{{status}}.vcf.gz', person=TRIO),
        tbis = expand('trio/{{genome}}/_{person}/{{bam_flag}}/{{vcf_flag}}/cov_{{min_cov}}'
            '.{{status}}.vcf.gz.tbi', person=TRIO),
    output:
        'trio/{genome}/{trio_name}/{bam_flag}/{vcf_flag}/cov_{min_cov}'
            '.{status,(filtered|failed)}.vcf.gz',
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} vcfmerge -o {output} {input.vcfs}'

rule discard_passing_snvs:
    message: 'Discard passing SNVs for {wildcards.person}'
    input:
        variants = 'trio/{genome}/_{person}/{bam_flag}/{vcf_flag}/cov_{min_cov}.filtered.vcf.gz',
        tbi = 'trio/{genome}/_{person}/{bam_flag}/{vcf_flag}/cov_{min_cov}.filtered.vcf.gz.tbi',
    output:
        'trio/{genome}/_{person}/{bam_flag}/{vcf_flag}/cov_{min_cov}.failed.vcf.gz',
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} vcffilter -r "PASS","." -i {input.variants} -o {output}'

rule filter_snvs:
    message: 'Filter SNVs for {wildcards.person}'
    input:
        variants = 'trio/{genome}/_{person}/{bam_flag}/{vcf_flag}/cov_{min_cov}.vcf.gz',
        tbi = 'trio/{genome}/_{person}/{bam_flag}/{vcf_flag}/cov_{min_cov}.vcf.gz.tbi',
    output:
        'trio/{genome}/_{person}/{bam_flag}/{vcf_flag}/cov_{min_cov}.filtered.vcf.gz',
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} vcffilter -g 50 --keep-expr "INFO.AQ > 7 && '
            'INFO.MB < 0.05 && INFO.MF < 0.1" --fail=fail -i {input.variants} -o {output}'

rule discard_snvs_low_cov:
    message: 'Discard SNVs with coverage < {wildcards.min_cov} in any dataset for {wildcards.person}'
    input:
        regions = 'trio/{genome}/%s/{bam_flag}/cov_{min_cov}.intersect.bed' % TRIO_NAME,
        variants = 'data/{person}/{genome}.{bam_flag}.{vcf_flag}.vcf.gz',
        fa_index = 'genome/{genome}.fa.fai',
    output:
        'trio/{genome}/_{person}/{bam_flag}/{vcf_flag}/cov_{min_cov,\d+}.vcf.gz'
    shell:
        '''
        {BEDTOOLS} intersect -u -wa -header -sorted -g {input.fa_index} \
            -a {input.variants} -b {input.regions} | \
        {BEDTOOLS} sort -g {input.fa_index} -header -i - | \
        bgzip -c > {output}
        '''

rule intersect_trio_cov_regions:
    message: 'Intersect coverage bed for the trio'
    input:
        child_bed = 'trio/{genome}/_%s/{bam_flag}/cov_{min_cov}.bed' % CHILD,
        father_bed = 'trio/{genome}/_%s/{bam_flag}/cov_{min_cov}.bed' % FATHER,
        mother_bed = 'trio/{genome}/_%s/{bam_flag}/cov_{min_cov}.bed' % MOTHER,
        fa_index = 'genome/{genome}.fa.fai',
    output:
        'trio/{genome}/{trio_name}/{bam_flag}/cov_{min_cov}.intersect.bed'
    shell:
        '''
        {BEDTOOLS} intersect -sorted -g {input.fa_index} -a {input.father_bed} -b {input.mother_bed} | \
        {BEDTOOLS} intersect -sorted -g {input.fa_index} -a - -b {input.child_bed} > {output}
        '''
        # '''
        # {BEDTOOLS} intersect -sorted -g {input.fa_index} -a {input.father_bed} -b {input.mother_bed} | \
        # {BEDTOOLS} sort -faidx {input.fa_index} -i - | \
        # {BEDTOOLS} intersect -sorted -g {input.fa_index} -a - -b {input.child_bed} | \
        # {BEDTOOLS} sort -faidx {input.fa_index} -i - > {output}
        # '''

rule intersect_trio_regions:
    message: 'Intersect {wildcards.regions} for the trio'
    input:
        child_bed = 'data/%s/{genome}.{regions}_regions.bed' % CHILD,
        father_bed = 'data/%s/{genome}.{regions}_regions.bed' % FATHER,
        mother_bed = 'data/%s/{genome}.{regions}_regions.bed' % MOTHER,
        fa_index = 'genome/{genome}.fa.fai',
    output:
        'trio/{genome}/{trio_name}/{regions}.intersect.bed'
    shell:
        '''
        {BEDTOOLS} intersect -a {input.father_bed} -b {input.mother_bed} | \
        {BEDTOOLS} intersect -a - -b {input.child_bed} | \
        {BEDTOOLS} sort -faidx {input.fa_index} -i - > {output}
        '''

rule nonconfident_bed:
    message: 'Create nonconfident regions bed for {wildcards.person}'
    input:
        confident = 'data/{person}/{genome}.confident_regions.bed',
        whole_genome = 'genome/regions/{genome}.whole_genome.bed',
    output: 'data/{person}/{genome}.nonconfident_regions.bed'
    shell: '{BEDTOOLS} subtract -a {input.whole_genome} -b {input.confident} > {output}'

rule combine_coverage_bed:
    message: 'Combine coverage bed for {wildcards.person}'
    input: expand('trio/{{genome}}/_{{person}}/{{bam_flag}}/{chrom}.cov_{{min_cov}}.bed',\
        chrom=chroms)
    output: 'trio/{genome}/_{person}/{bam_flag}/cov_{min_cov}.bed'
    shell: 'cat {input} > {output}'

rule generate_coverage_bed:
    message: 'Generate bed file with coverage >= {wildcards.min_cov} for chromosome '
        '{wildcards.chrom} for {wildcards.person}'
    input:
        bam = 'data/{person}/{genome}.{bam_flag}.bam',
        _bai = 'data/{person}/{genome}.{bam_flag}.bam.bai',
    output:
        'trio/{genome}/_{person}/{bam_flag}/{chrom}.cov_{min_cov}.bed',
    shell:
        '''
        {SAMTOOLS} view -F 3844 -q 30 {input.bam} {CHROM_PREFIX}{wildcards.chrom} -hb | \
        {BEDTOOLS} genomecov -bg -ibam - | \
        awk '$4 > {wildcards.min_cov}' | \
        {BEDTOOLS} merge -i - > {output}
        '''
