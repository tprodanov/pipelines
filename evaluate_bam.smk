# Variables: genome, person, bam_flags (separated by ",". First flag is supposed to have the most reads),
#       genes (true|false)
#       bedtools, samtools, rtgtools

import os

if 'genes' not in config.items():
    GENES = False
else:
    GENES = config['genes'] == 'true'

for key, value in config.items():
    globals()[key.upper()] = value

include: "common.smk"

RTG_MEM = config.get('rtg_mem', '12g')

if GENES:
    REGIONS_DIR = 'genome/regions/genes9'
    REGIONS = 'IKBKG MRC1 NCF1 OPN1MW OTOA PDPK1 RSPH10B SMN1 STRC'.split()
else:
    REGIONS_DIR = 'genome/segm_dup'
    THRESHOLDS = list(range(900, 1000, 10)) + [995, 999, 1000]
    LENGTH = '10kb'
    REGIONS = ['%s.%d-%d' % (LENGTH, THRESHOLDS[i], THRESHOLDS[i + 1]) for i in range(len(THRESHOLDS) - 1)]
    REGIONS.append('similar')

MAPQ_THRESHOLD = 30
CORRECT_THRESHOLD = 60
BAM_FLAGS = BAM_FLAGS.split(',')

DIR = os.path.dirname(workflow.snakefile)

# Input
#     genome/{genome}.fa
#     genome/segm_dup/{genome}.{region}.bed or genome/regions/genes9/{genome}.{gene_name}.bed
#     data/{person}/{genome}.{bam_flag}.bam
# Output
#     data/{person}/{genome}.bam_eval.csv or data/{person}/{genome}.genes_eval.csv
#     data/{person}/{genome}.reads.bed.gz
#     simeval_tmp/{person}/*

rule all:
    input:
        expand('data/{person}/{genome}.{prefix}_eval.csv', person=PERSON, genome=GENOME,
            prefix='genes' if GENES else 'bam'),

rule combine_bam_eval:
    input:
        expand('simeval_tmp/{{person}}/{{genome}}.{bam_flag}.{region}.bam.csv', bam_flag=BAM_FLAGS, region=REGIONS),
    output:
        'data/{person,[^/]+}/{genome,[^.]+}.%s_eval.csv' % ('genes' if GENES else 'bam')
    shell:
        '''
        echo "# Bam flags: {BAM_FLAGS}" >> {output}
        echo "# Mapq threshold: {MAPQ_THRESHOLD}, Correct threshold: {CORRECT_THRESHOLD}" >> {output}
        echo -e "bam_flag\tregion\tmapq\tsimulated\tmapped\tcorrect" >> {output}
        cat {input} >> {output}
        '''

rule bam_eval:
    input:
        bam = 'data/{person}/{genome}.{bam_flag}.bam',
        _bai = 'data/{person}/{genome}.{bam_flag}.bam.bai',
        reads_bed = 'data/{person}/{genome}.reads.bed.gz',
        _tbi = 'data/{person}/{genome}.reads.bed.gz.tbi',
        regions = '%s/{genome}.{region}.bed' % REGIONS_DIR,
    output:
        'simeval_tmp/{person,[^/]+}/{genome,[^.]+}.{bam_flag,[^.]+}'\
            '.{region}.bam.csv'
    shell:
        '''
        simulated="$({BEDTOOLS} intersect -wa -a {input.reads_bed} -b {input.regions} | cut -f4 | sort | uniq | wc -l)"
        {DIR}/count_reads.py -i {input.bam} -b {input.regions} -c {CORRECT_THRESHOLD} -o - | tail -n+2 | \
            awk -v simulated="$simulated" \
            '$1 == 0 || $1 == {MAPQ_THRESHOLD} {{ OFS="\t"; print "{wildcards.bam_flag}","{wildcards.region}",$1,simulated,$2,$3 }}' \
            >> {output}
        '''

rule reads_to_bed:
    input:
        bam = 'data/{person}/{genome}.%s.bam' % BAM_FLAGS[0],
        fai = 'genome/{genome}.fa.fai'
    output:
        'data/{person}/{genome}.reads.bed.gz'
    shell:
        r'''
        {SAMTOOLS} view -F 3844 {input.bam} | \
            cut -f1 | \
            sed 's/\(Read_[0-9]\+\)__.*Span=\([^:]\+\):\([^/]\+\).*/\2-\3-\1/' | \
            awk 'BEGIN{{FS="-"; OFS="\t"}} {{print $1,$2-1,$3,$4}}' | \
            {BEDTOOLS} sort -i - -faidx {input.fai} | \
            bgzip > {output}
        '''
