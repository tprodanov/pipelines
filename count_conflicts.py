#!/usr/bin/env python3

import pysam
import argparse
import csv
import collections
import numpy as np
import sys
import itertools


class Psv:
    def __init__(self, line):
        line = line.strip().split('\t')
        self.chrom = line[0]
        self.start = int(line[1])
        self.alleles = [line[2]] + line[3].split(',')
        self.gt0 = float(line[4])
        self.gt1 = float(line[5])

        self.genotypes = line[6]
        self.db = line[8]
        self.support = [0] * (len(self.alleles) + 1)

    @property
    def end(self):
        return self.start + len(self.alleles[0])

    def __lt__(self, other):
        (self.chrom, self.start) < (other.chrom, other.start)

    def write(self, writer, flag):
        row = dict(chrom=self.chrom, start=self.start, ref=self.alleles[0],
            alts=','.join(self.alleles[1:]), gt1_is_zero='%.2f' % self.gt0, gt2_is_zero='%.2f' % self.gt1,
            genotypes=self.genotypes, db=self.db, flag=flag, support=','.join(map(str, self.support)))
        writer.writerow(row)
    
    def clear(self):
        self.support = [0] * (len(self.alleles) + 1)


def find_start(psvs, start, lo=0, hi=None):
    if hi is None:
        hi = len(psvs)

    while lo < hi:
        mid = (lo + hi) // 2
        if start >= psvs[mid].end:
            lo = mid + 1
        else:
            hi = mid
    return lo


def find_end(psvs, end, lo=0, hi=None):
    if hi is None:
        hi = len(psvs)

    while lo < hi:
        mid = (lo + hi) // 2
        if end <= psvs[mid].start:
            hi = mid
        else:
            lo = mid + 1
    return lo


class Psvs:
    def __init__(self):
        self.chromosomes = collections.defaultdict(list)

    def add(self, psv):
        self.chromosomes[psv.chrom].append(psv)

    def sort(self):
        for chrom in self.chromosomes.values():
            chrom.sort()

    def get_psvs(self, chrom, start, end):
        chrom_psvs = self.chromosomes[chrom]
        i = find_start(chrom_psvs, start)
        j = find_end(chrom_psvs, end, lo=i)
        return chrom_psvs[i:j]


def load_merged_psvs(inp):
    while next(inp).startswith('#'):
        pass

    psvs = Psvs()
    for line in inp:
        psvs.add(Psv(line))
    psvs.sort()
    return psvs


def align(seq1, seq2):
    m = len(seq1) + 1
    n = len(seq2) + 1
    a = np.zeros((m, n), dtype=int)
    a[:, 0] = range(m)
    a[0, :] = range(n)

    for i in range(1, m):
        for j in range(1, n):
            a[i, j] = min(a[i, j - 1] + 1, a[i - 1, j] + 1,
                a[i - 1, j - 1] + (1 if seq1[i - 1] != seq2[j - 1] else -1))
    return a[m - 1, n - 1]


def process_read(query, flag, psvs, writer, reference, padding):
    intersecting = psvs.get_psvs(query.reference_name, query.reference_start, query.reference_end)
    if not intersecting:
        return

    ref_query = {}
    last_q_pos = None
    for q_pos, r_pos in query.get_aligned_pairs():
        if r_pos is None:
            continue
        if q_pos is not None:
            last_q_pos = q_pos
        ref_query[r_pos] = last_q_pos
    ref_query[query.reference_end] = query.query_alignment_end

    psvs = 0
    psvs_hom = 0
    conflicts_hom = 0
    psvs00 = 0
    conflicts00 = 0
    dbs = set()
    for psv in intersecting:
        start = max(query.reference_start, psv.start - padding)
        end = min(query.reference_end, psv.end + padding)
        if start > psv.start or end < psv.end:
            continue

        prefix = reference.fetch(query.reference_name, start, psv.start)
        suffix = reference.fetch(query.reference_name, psv.end, end)
        rec_seq = query.query_sequence[ref_query[start]:ref_query[end]]

        best_alleles = []
        best_value = sys.maxsize
        for i, allele in enumerate(psv.alleles):
            allele_seq = prefix + allele + suffix
            value = align(rec_seq, allele_seq)
            if value < best_value:
                best_value = value
                best_alleles.clear()
                best_alleles.append(min(i, 1))
            elif value == best_value:
                best_alleles.append(i)

        psvs += 1
        if psv.gt0 < 50 and psv.gt1 < 50:
            psvs_hom += 1
            # Homozygous 1/1
            if best_alleles == [0]:
                conflicts_hom += 1
        elif psv.gt0 >= 50 and psv.gt1 >= 50:
            psvs_hom += 1
            # Homozygous 0/0
            if 0 not in best_alleles:
                conflicts_hom += 1

        if psv.gt0 == 100 and psv.gt1 == 100:
            psvs00 += 1
            conflicts00 += 0 not in best_alleles
        if len(best_alleles) == 1:
            psv.support[best_alleles[0]] += 1
        else:
            psv.support[-1] += 1
        dbs.add(psv.db)

    row = dict(name=query.query_name, flag=flag, mapq=query.mapping_quality,
        chrom=query.reference_name, start=query.reference_start, end=query.reference_end,
        db=','.join(sorted(dbs)), psvs=psvs, psvs_hom=psvs_hom, conflicts_hom=conflicts_hom,
        psvs00=psvs00, conflicts00=conflicts00)
    writer.writerow(row)


def main():
    parser = argparse.ArgumentParser()
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-b', '--bams', metavar='STR=FILE', required=True, nargs='+',
        help='Multiple input BAM files in format "--bams flag2=path2 flag2=path2"')
    io_args.add_argument('-p', '--psvs', metavar='FILE', required=True,
        help='Input CSV file with merged PSVs', type=argparse.FileType())
    io_args.add_argument('-r', '--reference', metavar='FILE', required=True,
        help='Input reference fasta')
    io_args.add_argument('-R', '--reads-csv', metavar='FILE', required=True,
        help='Output CSV file with information about reads', type=argparse.FileType('w'))
    io_args.add_argument('-P', '--psvs-csv', metavar='FILE', required=True,
        help='Output CSV file with information about PSVs', type=argparse.FileType('w'))

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('--padding', metavar='INT', type=int, default=5,
        help='Padding around PSV used for realignment [%(default)s]')
    opt_args.add_argument('-c', '--chrom', metavar='STR', type=str,
        help='Run only on one chromosome')
    args = parser.parse_args()

    reads_outp = args.reads_csv
    reads_outp.write('# %s\n' % ' '.join(sys.argv))
    reads_writer = csv.DictWriter(reads_outp,
        fieldnames='name flag mapq chrom start end db psvs psvs_hom conflicts_hom psvs00 conflicts00'.split(),
        delimiter='\t', lineterminator='\n', quoting=csv.QUOTE_NONE)
    reads_writer.writeheader()
    
    psvs_outp = args.psvs_csv
    psvs_outp.write('# %s\n' % ' '.join(sys.argv))
    psvs_writer = csv.DictWriter(psvs_outp,
        fieldnames='chrom start ref alts gt1_is_zero gt2_is_zero genotypes db flag support'.split(),
        delimiter='\t', lineterminator='\n', quoting=csv.QUOTE_NONE)
    psvs_writer.writeheader()

    psvs = load_merged_psvs(args.psvs)
    reference = pysam.FastaFile(args.reference)
 
    for entry in args.bams:
        flag, path = entry.split('=')
        bam_file = pysam.AlignmentFile(path)
        try:
            inp = bam_file.fetch(args.chrom) if args.chrom else bam_file
        except ValueError:
            break

        for i, query in enumerate(inp):
            if query.flag & 3844 == 0:
                process_read(query, flag, psvs, reads_writer, reference, args.padding)
                if i % 100 == 0:
                    reads_outp.flush()

        psvs1 = psvs.chromosomes[args.chrom] if args.chrom else itertools.chain(*psvs.chromosomes.values())
        for i, psv in enumerate(psvs1):
            psv.write(psvs_writer, flag)
            psv.clear()
            if i % 100 == 0:
                psvs_outp.flush()


if __name__ == '__main__':
    main()
