#!/usr/bin/env python3

import pysam
import re
import os


def rename_reads(filename, count, outp):
    m = re.match('(\w+).hapl([12]).sam', os.path.basename(filename))
    if m is None:
        print('Failed to parse filename "%s"' % filename)
        exit(1)
    chrom = m.group(1)
    hapl = 'ab'[int(m.group(2)) - 1]

    inp = pysam.AlignmentFile(filename)
    for query in inp:
        name = 'Read_{name}__hapl={hapl};Span={chrom}:{start}-{end}/0/0_{length}'\
            .format(name=count, hapl=hapl, chrom=chrom, start=query.reference_start + 1,
                end=query.reference_end, length=query.query_length)
        outp.write('@%s\n%s\n+\n%s\n' % (name, query.query_sequence, query.qual))
        count += 1
    return count


def main():
    import argparse
    parser = argparse.ArgumentParser(
        description='Rename reads generated using simlord',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i FILE -o FILE')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='FILE', required=True, nargs='+',
        help='Multiple input sam files. File names should match "{chrom}.hapl{hapl}.sam"')
    io_args.add_argument('-o', '--output', metavar='FILE', type=argparse.FileType('w'),
        help='Output fastq file', required=True)

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    count = 0
    for filename in args.input:
        count = rename_reads(filename, count, args.output)


if __name__ == '__main__':
    main()
