# Variables: bedtools, samtools, rtgtools,
#       genome, person, bam_flags (separated by ,), vcf_flags (separated by ,)

import os
for key, value in config.items():
    globals()[key.upper()] = value

include: "common.smk"

RTG_MEM = config.get('rtg_mem', '12g')

THRESHOLDS = list(range(900, 1000, 10)) + [995, 999, 1000]
LENGTH = '10kb'
BINS = ['%s.%d-%d' % (LENGTH, THRESHOLDS[i], THRESHOLDS[i + 1]) for i in range(len(THRESHOLDS) - 1)]
BINS.append('similar')

QUAL_THRESHOLD = 30
BAM_FLAGS = BAM_FLAGS.split(',')
VCF_FLAGS = VCF_FLAGS.split(',')

DIR = os.path.dirname(workflow.snakefile)

# Input
#     genome/{genome}.fa
#     genome/segm_dup/{genome}.{length}.{bin}.bed
#     genome/segm_dup/{genome}.similar.bed
#     data/{person}/{genome}.{bam_flag}.{vcf_flag}.vcf.gz
#     data/{person}/{genome}.truth.vcf.gz
# Output
#     data/{person}/{genome}.vcf_eval.csv
#     simeval_tmp/{person}/*

rule all:
    input:
        expand('data/{person}/{genome}.vcf_eval.csv', person=PERSON, genome=GENOME),

rule combine_vcf_eval:
    input:
        expand('simeval_tmp/{{person}}/{{genome}}.{bam_flag}.{vcf_flag}.{region}/eval.csv',\
            bam_flag=BAM_FLAGS, vcf_flag=VCF_FLAGS, region=BINS),
    output:
        'data/{person}/{genome,[^.]+}.vcf_eval.csv'
    shell:
        'echo "# Length: {LENGTH}, score threshold: {QUAL_THRESHOLD}" > {output}\n'
        'echo "# Bam flags: {BAM_FLAGS}, Vcf flags: {VCF_FLAGS}" >> {output}\n'
        'echo -e "bam_flag\tvcf_flag\tregion\tscore\ttrue_positives_baseline\tfalse_positives\t'
        'true_positives_call\tfalse_negatives\tprecision\tsensitivity\tf_measure" >> {output}\n'
        'cat {input} >> {output}'

rule extract_roc:
    input:
        'simeval_tmp/{person}/{genome}.{bam_flag}.{vcf_flag}.{region}/snp_roc.tsv.gz'
    output:
        temp('simeval_tmp/{person}/{genome,[^.]+}.{bam_flag,[^.]+}.{vcf_flag,[^.]+}.'\
            '{region}/eval.csv')
    shell:
        '''
        printf "{wildcards.bam_flag}\t{wildcards.vcf_flag}\t{wildcards.region}\t" > {output}
        zcat {input} | awk '$1 >= {QUAL_THRESHOLD}' | tail -n1 >> {output}
        printf "{wildcards.bam_flag}\t{wildcards.vcf_flag}\t{wildcards.region}\t" >> {output}
        zcat {input} | tail -n1 >> {output}
        '''

rule vcf_eval:
    input:
        vcf = 'data/{person}/{genome}.{bam_flag}.{vcf_flag}.vcf.gz',
        vcf_tbi = 'data/{person}/{genome}.{bam_flag}.{vcf_flag}.vcf.gz.tbi',
        truth = 'data/{person}/{genome}.truth.vcf.gz',
        truth_tbi = 'data/{person}/{genome}.truth.vcf.gz.tbi',
        genome = 'genome/{genome}.sdf',
        regions = 'genome/segm_dup/{genome}.{region}.bed',
    output:
        'simeval_tmp/{person}/{genome,[^.]+}.{bam_flag,[^.]+}.{vcf_flag,[^.]+}.{region}/snp_roc.tsv.gz'
    params:
        out_dir = lambda wildcards, output: os.path.dirname(output[0])
    shell:
        '''
        rm -r {params.out_dir}
        {RTGTOOLS} vcfeval -c {input.vcf} -b {input.truth} -t {input.genome} -o {params.out_dir} \
--bed-regions {input.regions}
        '''
