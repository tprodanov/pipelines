# Variables:
#       rtgtools, [rtg_mem]
#       genome, person, dirname, bam_flag, vcf_flag, coverage=20

# Input:
#     genome/{genome}.fa
#     genome/segm_dup/{genome}.similar.bed
#     genome/regions/{genome}.genes.bed
#     db/{genome}/psvs.vcf.gz
#     data/{person}/{genome}.{bam_flag}_before.bam
#     data/{person}/{genome}.{bam_flag}_before.{vcf_flag}.vcf.gz
#     realignment/{person}/{dirname}/final.sort.bam
#     realignment/{person}/{dirname}/{vcf_flag}.vcf.gz
# Output:
#     realignment/{person}/{dirname}/{vcf_flag}_metrics


for key, value in config.items():
    globals()[key.upper()] = value

RTG_MEM = config.get('rtg_mem', '12g')
COVERAGE = config.get('coverage', '20')
DIR = os.path.dirname(workflow.snakefile)

def get_chrom_prefix(genome):
    if genome == 'hg37':
        return ''
    elif genome == 'hg38':
        return 'chr'
    else:
        print('Unexpected genome version. Expected hg37|hg38')
        exit(1)
CHROM_PREFIX = get_chrom_prefix(GENOME)
CHROMS = [int(n + 1) for n in range(22)] + ['X', 'Y']

rule all:
    input: expand('realignment/{person}/{dirname}/{vcf_flag}_metrics/{flag}.{regions}.csv',\
        person=PERSON, dirname=DIRNAME, vcf_flag=VCF_FLAG, flag=['before', 'after'],\
        regions=['similar', 'diverse', 'genes'])


def select_regions(wildcards):
    regions = wildcards.regions
    if regions == 'similar' or regions == 'diverse':
        return 'genome/segm_dup/{genome}.{regions}.bed'.format(genome=GENOME, regions=regions)
    elif regions == 'genes':
        return 'genome/regions/{genome}.genes.bed'.format(genome=GENOME)
    print('Unexpected regions %s' % regions)
    exit(1)


rule index_tbi:
    message: 'Index "{wildcards.path}.vcf.gz"'
    input: '{path}.vcf.gz'
    output: '{path}.vcf.gz.tbi'
    shell: '{TABIX} -p vcf {input}'


rule whole_genome_bed:
    message: 'Generate bed file with the whole genome'
    input: 'genome/{genome}.fa.fai'
    output: 'genome/regions/{genome}.whole_genome.bed'
    shell: 'awk \'OFS="\t" {{print $1,0,$2}}\' {input} > {output}'


rule count_variants:
    input:
        variants = 'realignment/{person}/{dirname}/{vcf_flag}_metrics/intersected_{flag}.filt.vcf.gz',
        tbi = 'realignment/{person}/{dirname}/{vcf_flag}_metrics/intersected_{flag}.filt.vcf.gz.tbi',
        regions = select_regions,
        covered = 'realignment/{person}/{dirname}/{vcf_flag}_metrics/cov/{flag}.cov%s.bed' % COVERAGE,
    output:
        'realignment/{person}/{dirname}/{vcf_flag}_metrics/{flag}.{regions}.csv'
    shell:
        '{DIR}/metrics/count_variants.py -v {input.variants} -r {input.regions} '
            '-c {input.covered} -o {output}'


rule diverse_regions:
    input:
        whole_genome = 'genome/regions/{genome}.whole_genome.bed',
        similar = 'genome/segm_dup/{genome}.similar.bed',
    output:
        'genome/segm_dup/{genome}.diverse.bed'
    shell:
        'head -n24 {input.whole_genome} | {BEDTOOLS} subtract -a - -b {input.similar} > {output}'


rule filter_variants:
    input:
        variants = 'realignment/{person}/{dirname}/{vcf_flag}_metrics/intersected_{flag}.vcf.gz',
        regions = 'realignment/{person}/{dirname}/{vcf_flag}_metrics/cov/{flag}.cov%s.bed'\
            % COVERAGE,
    output:
        'realignment/{person}/{dirname}/{vcf_flag}_metrics/intersected_{flag}.filt.vcf.gz',
    shell:
        '{RTGTOOLS} RTG_MEM={RTG_MEM} vcffilter --include-bed={input.regions} -q 30 -k PASS '
            '-i {input.variants} -o {output}'


rule split_psvs:
    input: 'db/{genome}/psvs.vcf.gz'
    output: 'db/{genome}/psvs-split.vcf.gz'
    shell:
        '{DIR}/metrics/split_variants.py -i {input} -o {output}'


def get_variants(wildcards):
    if wildcards.flag == 'before':
        return 'data/{person}/{genome}.{bam_flag}_before.{vcf_flag}.vcf.gz'\
            .format(person=PERSON, genome=GENOME, bam_flag=BAM_FLAG, vcf_flag=VCF_FLAG)
    elif wildcards.flag == 'after':
        return 'realignment/{person}/{dirname}/{vcf_flag}.vcf.gz'\
            .format(person=PERSON, dirname=DIRNAME, vcf_flag=VCF_FLAG)
    print('Unexpected flag %s' % wildcards.flag)
    exit(1)

rule intersect_with_psvs:
    input:
        genome = 'genome/{genome}.fa'.format(genome=GENOME),
        psvs = 'db/{genome}/psvs-split.vcf.gz'.format(genome=GENOME),
        psvs_tbi = 'db/{genome}/psvs-split.vcf.gz.tbi'.format(genome=GENOME),
        variants = get_variants,
    output:
        'realignment/{person}/{dirname}/{vcf_flag}_metrics/intersected_{flag,[^.]+}.vcf.gz',
    shell:
        '{DIR}/metrics/intersect_variants_psvs.py -p {input.psvs} -v {input.variants} '
            '-r {input.genome} -o - | bgzip -c > {output}'


def get_input_bam(wildcards):
    if wildcards.flag == 'before':
        return 'data/{person}/{genome}.{bam_flag}_before.bam'\
            .format(person=PERSON, genome=GENOME, bam_flag=BAM_FLAG)
    elif wildcards.flag == 'after':
        return 'realignment/{person}/{dirname}/final.sort.bam'\
            .format(person=PERSON, dirname=DIRNAME)
    print('Unexpected flag %s' % wildcards.flag)
    exit(1)


rule combine_coverage_bed:
    input: expand('realignment/{{person}}/{{dirname}}/{{vcf_flag}}_metrics'\
        '/cov/{{flag}}.{chrom}.cov{{cov}}.bed', chrom=CHROMS)
    output: 'realignment/{person}/{dirname}/{vcf_flag}_metrics/cov/{flag,[^.]+}.cov{cov}.bed'
    shell: 'cat {input} > {output}'


rule generate_coverage_bed:
    input:
        get_input_bam,
    output: # TODO ADD temp()
        'realignment/{person}/{dirname}/{vcf_flag}_metrics/cov/{flag,[^.]+}.{chrom}.cov{cov}.bed',
    shell:
        '''
        ({SAMTOOLS} view -F 3844 -q 30 {input} {CHROM_PREFIX}{wildcards.chrom} -hb | \
        {BEDTOOLS} genomecov -bg -ibam - || true) | \
        awk '$4 > {wildcards.cov}' | \
        {BEDTOOLS} merge -i - > {output}
        '''
