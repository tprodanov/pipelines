#!/usr/bin/env python3

import random
import pysam
import itertools
import numpy.random
import sys


# estimate prior probability of genotypes using strategy described here:
# http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2694485/
# "prior probability of each genotype"

def create_phased_genotype_selector(args):
    alleles = 'ACGT'
    genotypes = list(itertools.combinations_with_replacement(alleles, 2))

    het_snp_rate = args.het_snp_rate
    hom_snp_rate = args.hom_snp_rate

    diploid_genotype_priors = {}
    haploid_genotype_priors = {}
    transition = dict(['AG', 'GA', 'TC', 'CT'])

    for allele in alleles:
        # priors on haploid alleles
        haploid_genotype_priors[allele] = {}
        haploid_genotype_priors[allele][allele] = 1 - het_snp_rate
        haploid_genotype_priors[allele][transition[allele]] = het_snp_rate / 6 * 4
        for transversion in alleles:
            if transversion not in haploid_genotype_priors[allele]:
                haploid_genotype_priors[allele][transversion] =  het_snp_rate / 6

        diploid_genotype_priors[allele] = []
        for genotype in genotypes:
            g1, g2 = genotype
            # probability of homozygous reference is the probability of neither het or hom SNP
            if g1 == g2 and g1 == allele:
                diploid_genotype_priors[allele].append(0)
            elif g1 == g2 and g1 != allele:
                # transitions are 4 times as likely as transversions
                if g1 == transition[allele]:
                    diploid_genotype_priors[allele].append(hom_snp_rate / 6 * 4)
                else:
                    diploid_genotype_priors[allele].append(hom_snp_rate / 6)
            else: # else it's the product of the haploid priors
                    diploid_genotype_priors[allele].append(
                        haploid_genotype_priors[allele][g1] * haploid_genotype_priors[allele][g2])

        # remove the option of selecting homozygous reference
        total = sum(diploid_genotype_priors[allele])
        for i in range(len(genotypes)):
            diploid_genotype_priors[allele][i] /= total
        # make sure everything sums to 1
        diploid_genotype_priors[allele][-1] = 1.0 - sum(diploid_genotype_priors[allele][:-1])

    g_ixs = list(range(len(genotypes)))
    def phased_genotype_selector(ref_nt):
        g_ix = numpy.random.choice(g_ixs, 1, p=diploid_genotype_priors[ref_nt])[0]
        g = genotypes[g_ix]
        if random.random() > 0.5:
            return g
        else:
            return (g[1], g[0])
    return phased_genotype_selector


def simulate_snvs_vcf(genome, outp, args):
    chromosomes = {'%s%s' % (args.chrom_prefix, name) for name in list(range(1, 23)) + ['X', 'Y']}

    phased_genotype_selector = create_phased_genotype_selector(args)
    header = pysam.VariantHeader()
    outp.write('##fileformat=VCFv4.2\n')
    outp.write('##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">\n')
    outp.write('##source=%s\n' % ' '.join(sys.argv))
    outp.write('#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\ttruth\n')

    for chrom in genome.references:
        if chrom not in chromosomes:
            continue

        size = genome.get_reference_length(chrom)
        pos = 0
        while pos < size:
            pos += numpy.random.geometric(args.pos_geom, size=None)
            ref_nt = genome.fetch(chrom, pos, pos + 1).upper()
            if ref_nt not in {'A', 'C', 'G', 'T'}:
                continue
            genotype = phased_genotype_selector(ref_nt)

            var_list = [ref_nt]
            gt_list = []
            for nt in genotype:
                if nt in var_list:
                    gt_list.append(var_list.index(nt))
                else:
                    gt_list.append(len(var_list))
                    var_list.append(nt)

            assert 'N' not in var_list
            if len(var_list) == 1:
                continue
            
            outp.write('{chrom}\t{pos}\t.\t{ref}\t{alt}\t100\tPASS\t.\tGT\t{gt}\n'
                .format(chrom=chrom, pos=pos + 1, ref=ref_nt, alt=','.join(var_list[1:]),
                    gt='|'.join(map(str, gt_list))))


def main():
    import argparse
    parser = argparse.ArgumentParser(
        description='Generate SNVs',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i FILE -o FILE [args]')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='FILE', required=True,
        help='Input genome in fasta format')
    io_args.add_argument('-o', '--output', metavar='FILE', type=argparse.FileType('w'),
        help='Output vcf file', required=True)

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('--chrom-prefix', metavar='STR', default='',
        help='Chromosomes prefix (using chromosome names like <STR>1, <STR>X) [%(default)r]')
    opt_args.add_argument('--hom-snp-rate', metavar='FLOAT', default=0.0005,
        help='Homozygous SNP rate [%(default)s]')
    opt_args.add_argument('--het-snp-rate', metavar='FLOAT', default=0.001,
        help='Heterozygous SNP rate [%(default)s]')
    opt_args.add_argument('--pos-geom', metavar='FLOAT', default=0.0015,
        help='Geometric distribution parameter that defines distance\n'
            'between positions [%(default)s]')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    simulate_snvs_vcf(pysam.FastaFile(args.input), args.output, args)


if __name__ == '__main__':
    main()
