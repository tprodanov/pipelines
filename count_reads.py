#!/usr/bin/env python3

import sys
import pysam
import re


def percentage_inside_true_location(true_location, aln_location):
    t_chrom, t_start, t_end = true_location
    a_chrom, a_start, a_end = aln_location
    if t_chrom != a_chrom:
        return 0
    intersection = max(0, min(a_end, t_end) - max(a_start, t_start))
    return 100 * intersection / (a_end - a_start)


def extract_true_location(read_name):
    m = re.search(extract_true_location.pattern, read_name)
    assert m is not None
    return m.group(1), int(m.group(2)) - 1, int(m.group(3))
extract_true_location.pattern = re.compile(r'=(\w+):(\d+)-(\d+)')


def perc_str(part, total):
    return '{}\t{:.2f}'.format(part, 100 * part / total if total != 0 else 0.0)


def count_reads(alignment_file, bed_regions, correct_threshold, outp):
    MAX_MAPQ = 60
    mapped = [0] * (MAX_MAPQ + 1)
    correct = [0] * (MAX_MAPQ + 1)

    analyzed_names = set()
    for line in bed_regions:
        reg_chrom, reg_start, reg_end = line.strip().split()[:3]
        for query in alignment_file.fetch(reg_chrom, int(reg_start), int(reg_end)):
            if query.is_unmapped or query.is_secondary or query.is_supplementary:
                continue
            if query.query_name in analyzed_names:
                continue
            analyzed_names.add(query.query_name)

            mapq = min(query.mapping_quality, MAX_MAPQ)
            mapped[mapq] += 1

            true_location = extract_true_location(query.query_name)
            p = percentage_inside_true_location(true_location,
                (query.reference_name, query.reference_start, query.reference_end))
            if p >= correct_threshold:
                correct[mapq] += 1

    outp.write('mapq\tmapped\tcorrect\n')
    total_mapped = 0
    total_correct = 0
    for mapq in range(MAX_MAPQ, -1, -1):
        total_mapped += mapped[mapq]
        total_correct += correct[mapq]
        outp.write('%d\t%d\t%d\n' % (mapq, total_mapped, total_correct))


def main():
    import argparse
    parser = argparse.ArgumentParser(
        description='Count the number of correct reads',
        formatter_class=argparse.RawTextHelpFormatter, add_help=False,
        usage='%(prog)s -i FILE -b FILE')
    io_args = parser.add_argument_group('Input/output arguments')
    io_args.add_argument('-i', '--input', metavar='FILE', required=True,
        help='Input indexed bam file')
    io_args.add_argument('-b', '--bed', metavar='FILE', required=True, type=argparse.FileType(),
        help='Input bed file')
    io_args.add_argument('-o', '--output', metavar='FILE', required=True, type=argparse.FileType('w'),
        help='Output csv file')

    opt_args = parser.add_argument_group('Optional arguments')
    opt_args.add_argument('-c', '--correct', metavar='INT', type=int, default=60,
        help='Portion of the alignment that should intersect the true location')

    other = parser.add_argument_group('Other arguments')
    other.add_argument('-h', '--help', action='help', help='Show this message and exit')
    args = parser.parse_args()

    count_reads(pysam.AlignmentFile(args.input), args.bed, args.correct, args.output)


if __name__ == '__main__':
    main()
