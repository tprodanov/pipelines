
def get_chrom_prefix(genome):
    if genome == 'hg37':
        return ''
    elif genome == 'hg38':
        return 'chr'
    else:
        print('Unexpected genome version. Expected hg37|hg38')
        exit(1)

rule index_bai:
    message: 'Index "{wildcards.path}.bam"'
    input: '{path}.bam'
    output: '{path}.bam.bai'
    shell: '{SAMTOOLS} index {input}'

rule index_fai:
    message: 'Index "{wildcards.path}.fa"'
    input: '{path}.fa'
    output: '{path}.fa.fai'
    shell: '{SAMTOOLS} faidx {input}'

rule index_tbi:
    message: 'Index "{wildcards.path}.{wildcards.ext}.gz"'
    input: '{path}.{ext}.gz'
    output: '{path}.{ext,[a-z]+}.gz.tbi'
    shell: '{TABIX} -p {wildcards.ext} {input}'

rule sdf_index:
    message: 'Create rtg index for "{wildcards.path}"'
    input: '{path}.fa'
    output: directory('{path}.sdf')
    shell: '{RTGTOOLS} RTG_MEM={RTG_MEM} format -o {output} {input}'

rule bgzip:
    message: 'bgzip "{wildcards.path}"'
    input: '{path}'
    output: '{path}.gz'
    shell: 'bgzip {input}'

rule whole_genome_bed:
    message: 'Generate bed file with the whole genome'
    input: 'genome/{genome}.fa.fai'
    output: 'genome/regions/{genome}.whole_genome.bed'
    shell: 'awk \'OFS="\t" {{print $1,0,$2}}\' {input} > {output}'
