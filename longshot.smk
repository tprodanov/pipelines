# Variables: samtools, longshot, genome, path, person, wdir
for key, value in config.items():
    globals()[key.upper()] = value

include: "common.smk"

CHROM_PREFIX = get_chrom_prefix(GENOME)
RTG_MEM = config.get('rtg_mem', '12g')

assert PATH.endswith('.bam')
PATH = PATH[:-4]

# Input
#     {path_wo_ext}.bam
#     genome/{genome}.fa'
# Output
#     {path_wo_ext}.longshot.vcf.gz[.tbi]

chroms = [int(n + 1) for n in range(22)] + ['X', 'Y']

ARGS = ' '.join(sys.argv)


if isinstance(WDIR, int):
    WDIR = '%03d' % WDIR


rule all:
    input:
        expand('{path}.longshot.vcf.gz{ext}', path=PATH, ext=['', '.tbi']),


rule combine:
    message: 'Combine calls from all chromosomes'
    input: expand('longshot_tmp/{wdir}/{chrom}.vcf', wdir=WDIR, chrom=chroms)
    output: '%s.longshot.vcf' % PATH
    shell:
        '''
        for f in {input}; do
            if [ -s $f ]; then
                grep '^#' $f | sed 's/INFO=\(.*\)Number=G/INFO=\\1Number=./' > {output}
                break
            fi
        done
        cat {input} | grep -v '^#' >> {output}
        '''

rule call_chromosome:
    message: 'Run longshot on chromosome {wildcards.chrom}'
    input:
        bam = '%s.bam' % PATH,
        genome = 'genome/%s.fa' % GENOME,
        _command = 'longshot_tmp/%s/command.txt' % WDIR,
    output:
        'longshot_tmp/%s/{chrom}.vcf' % WDIR,
    shell:
        '''
        {LONGSHOT} --bam {input.bam} --ref {input.genome} --out {output} \
            --region {CHROM_PREFIX}{wildcards.chrom} --sample_id {PERSON} --max_cov 200
        if ! [ -f "{output}" ]; then
            touch "{output}"
        fi
        '''


rule print_command:
    output: '{path}/command.txt',
    run:
        import sys
        with open(output[0], 'w') as outp:
            outp.write('%s\n' % ARGS)


