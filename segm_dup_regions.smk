# Variables: csvkit, bedtools, samtools, genome
for key, value in config.items():
    globals()[key.upper()] = value

THRESHOLDS = [0] + list(range(900, 1000, 10)) + [995, 999, 1000]
BINS = ['%s-%s' % (THRESHOLDS[i], THRESHOLDS[i + 1]) for i in range(len(THRESHOLDS) - 1)]

LENGTH = 10000
LENGTH_STR = '10kb'

# Input
#     genome/{genome}.fa.fai
#     genome/segm_dup/{genome}.csv
# Output
#     genome/segm_dup/{genome}.{length}.{bin}.bed'

include: "common.smk"

rule all:
    input:
        expand('genome/segm_dup/{genome}.{length}.{bin}.bed',\
            genome=GENOME, length=LENGTH_STR, bin=BINS)


def upper_bed(wildcards):
    i = THRESHOLDS.index(int(wildcards.upper))
    if i + 1 == len(THRESHOLDS):
        return 'genome/empty.bed'
    else:
        return 'genome/segm_dup/{genome}.{length}.s_ge_{similarity}.bed'\
            .format(genome=wildcards.genome, length=wildcards.length, similarity=wildcards.upper)


def lower_bed(wildcards):
    i = THRESHOLDS.index(int(wildcards.lower))
    if i == 0:
        return 'genome/regions/{genome}.whole_genome.bed'.format(genome=wildcards.genome)
    else:
        return 'genome/segm_dup/{genome}.{length}.s_ge_{similarity}.bed'\
            .format(genome=wildcards.genome, length=wildcards.length, similarity=wildcards.lower)


rule bed_equal:
    input:
        lower = lower_bed,
        upper = upper_bed,
        fa_index = 'genome/{genome}.fa.fai',
    output:
        'genome/segm_dup/{genome,[^.]+}.{length,[^.]+}.{lower,\d+}-{upper,\d+}.bed'
    shell:
        '{BEDTOOLS} subtract -a {input.lower} -b {input.upper} -g {input.fa_index} | '
        '{BEDTOOLS} sort -g {input.fa_index} > {output}'


rule bed_greater_equal:
    input:
        dup_csv = 'genome/segm_dup/{genome}.csv',
        fa_index = 'genome/{genome}.fa.fai',
        whole_genome = 'genome/regions/{genome}.whole_genome.bed',
    output:
        temp('genome/segm_dup/{genome,[^.]+}.{length,[^.]+}.s_ge_{similarity,\d+}.bed')
    shell:
        '{CSVKIT}cut -t -c chrom,chromStart,chromEnd,strand,fracMatch {input.dup_csv} | '
        '{CSVKIT}format -T | '
        'tail -n+2 | '
        "awk '$3 - $2 >= {LENGTH} && $5 >= {wildcards.similarity} / 1000' | "
        'cut -f1-3 | '
        '{BEDTOOLS} intersect -a - -b {input.whole_genome} | '
        '{BEDTOOLS} sort -g {input.fa_index} | '
        '{BEDTOOLS} merge -g {input.fa_index} > {output}'


rule make_empty:
    output:
        temp('genome/empty.bed')
    shell:
        '> {output}'